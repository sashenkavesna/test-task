
package testtask;

import java.util.List;
import java.util.Scanner;

/**
 * Class InputController serves for listening and processing user input.
 *
 * @author Александра
 * @version 1.0
 */
public class InputController {

    /**
     * Maximum quantity of goods for rent.
     */
    public static final int MAX_RENT = 3;

    private List<String> tableHeaders;
    private Shop shop;
    private Scanner scan;

    /**
     * Initialize.
     *
     * @param shop
     * @param tags All different tags in XML file
     */
    InputController(Shop shop, List<String> tags) {
        this.shop = shop;
        tableHeaders = tags;
    }

    /**
     * Listen and processing user input.
     */
    public void control() {
        while (true) {
            scan = new Scanner(System.in);
            switch (scan.next()) {
                case "1":
                    modifyTable(shop.getAllShopInfo());
                    break;
                case "2":
                    if (shop.getRent().isEmpty()) {
                        System.out.println(UIConstants.NO_RENT);
                        System.out.println(UIConstants.MENU);
                    } else {
                        modifyTable(shop.getAllRentInfo());
                    }
                    break;
                case "3":
                    System.out.println(UIConstants.MAX_RENT);
                    System.out.println(UIConstants.TITLE + UIConstants.OK);

                    String goodsTitle = scan.next();

                    if (!goodsTitle.equals(UIConstants.OK)) {
                        SportEquipment[] equipments = new SportEquipment[0];
                        RentUnit rentUnit = new RentUnit();
                        for (int i = 0; i < MAX_RENT; i++) {
                            if (goodsTitle.equals(UIConstants.OK)) {
                                break;
                            }
                            SportEquipment[] returned = searchInShop(equipments, goodsTitle);
                            if (returned != null) {
                                equipments = searchInShop(equipments, goodsTitle);
                            } else {
                                i--;
                            }
                            if (i != MAX_RENT - 1) {
                                goodsTitle = scan.next();
                            }
                        }
                        rentUnit.setUnit(equipments);
                        shop.addRent(rentUnit);
                    }
                    System.out.println(UIConstants.MENU);
                    break;
                case "4":
                    System.exit(0);
                    break;
                default:
                    System.out.println(UIConstants.INCORRECT);

            }
        }
    }

    /**
     * Create and modify table with current data.
     *
     * @param data List with rows
     */
    public void modifyTable(List<List<String>> data) {
        TableView table = new TableView();
        TableModel tableModel = new TableModel();
        for (String header : tableHeaders) {
            tableModel.addHeader(header);
        }
        tableModel.addData(data);
        table.setModel(tableModel);
        table.updateTable();
        System.out.println(UIConstants.MENU);
    }

    /**
     * Processing of input text for rent
     *
     * @param equipment      Current rent equipment
     * @param goodsTitle     Title of sport equipment
     * @return newEquipment  Rent equipment
     */
    private SportEquipment[] searchInShop(SportEquipment[] equipment, String goodsTitle) {
        int equipCount = equipment.length;
        SportEquipment[] newEquipment = null;
        SportEquipment finded = shop.searchGood(goodsTitle);
        if (finded == null || shop.getQuantity(finded) == 0) {
            System.out.println(UIConstants.NO_GOODS);
        } else {
            equipCount++;
            newEquipment = new SportEquipment[equipCount];
            System.arraycopy(equipment, 0, newEquipment, 0, equipment.length);
            newEquipment[newEquipment.length - 1] = finded;
            shop.remove(finded);
        }
        return newEquipment;
    }
}
