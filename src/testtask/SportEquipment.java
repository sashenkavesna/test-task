package testtask;

/**
 * Class SportEquipment contains information about an equipment.
 *
 * @author Александра
 * @version 1.0
 */
public class SportEquipment {

    private int price;
    private String title;
    private Category category;

    /**
     * Initialize price
     *
     * @param price
     */
    public void setPrice(int price) {
        this.price = price;
    }

    /**
     * Initialize title
     *
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Initialize Category
     *
     * @param category
     */
    public void setCategory(Category category) {
        this.category = category;
    }

    /**
     * @return price
     */
    public int getPrice() {
        return price;
    }

    /**
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @return category
     */
    public Category getCategory() {
        return category;
    }
}
