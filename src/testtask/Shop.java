
package testtask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class Shop contains information about goods wnich aviable for rent and
 * information about rent equipment.
 *
 * @author Александра
 * @version 1.0
 */
public class Shop {

    /**
     * All goods in shop
     */
    private Map<SportEquipment, Integer> goods;

    /**
     * All Rent equipment.
     */
    private List<RentUnit> rentEquipment;

    /**
     * Initialize
     */
    public Shop() {
        goods = new HashMap<>();
        rentEquipment = new ArrayList<>();
    }

    /**
     * Add goods in shop.
     *
     * @param equip Sport equipment to add
     * @param quantity Quantity of sport equipment
     */
    public void addSportEquipment(SportEquipment equip, Integer quantity) {
        goods.put(equip, quantity);
    }

    /**
     * Remove goods from shop.
     *
     * @param equipment Sport equipment to remove
     */
    public void remove(SportEquipment equipment) {
        goods.put(equipment, getQuantity(equipment) - 1);
    }

    /**
     * Add rent unit in rent.
     *
     * @param unit rent equipment by user
     */
    public void addRent(RentUnit unit) {
        rentEquipment.add(unit);
    }

    /**
     * @return rentEquipment All rent equipment by all users
     */
    public List<RentUnit> getRent() {
        return rentEquipment;
    }

    /**
     * Search current goods in shop by title.
     *
     * @params title Title of current equipment
     * @return SportEquipment Finded goods
     */
    public SportEquipment searchGood(String title) {
        SportEquipment finded = null;
        for (SportEquipment key : goods.keySet()) {
            if (key.getTitle().equals(title)) {
                finded = key;
                break;
            }
        }
        return finded;
    }

    /**
     * @return Quantity of current equipment
     */
    public int getQuantity(SportEquipment equip) {
        return goods.get(equip);
    }

    /**
     * Creates rows for table model of goods in shop.
     *
     * @return allInf Rows contains info about equipment
     */
    public List<List<String>> getAllShopInfo() {
        List<List<String>> allInf = new ArrayList<>();
        for (Map.Entry<SportEquipment, Integer> entry : goods.entrySet()) {
            List<String> inf = new ArrayList<>();
            inf.add(entry.getKey().getTitle());
            inf.add(String.valueOf(entry.getKey().getPrice()));
            inf.add(String.valueOf(entry.getValue()));
            inf.add(entry.getKey().getCategory().getType());
            inf.add(entry.getKey().getCategory().getSport());
            if (entry.getValue() != 0) {
                allInf.add(inf);
            }
        }
        return allInf;
    }

    /**
     * Creates rows for table model of rent.
     *
     * @return allInf Rows contains info about rent
     */
    public List<List<String>> getAllRentInfo() {
        List<List<String>> allInf = new ArrayList<>();
        for (RentUnit unit : rentEquipment) {
            for (SportEquipment equip : unit.getUnit()) {
                List<String> inf = new ArrayList<>();
                inf.add(equip.getTitle());
                inf.add(String.valueOf(equip.getPrice()));
                inf.add(String.valueOf(unit.getQuantity(equip)));
                inf.add(equip.getCategory().getType());
                inf.add(equip.getCategory().getSport());
                allInf.add(inf);
            }
        }
        return allInf;
    }
}
