package testtask;

import java.util.ArrayList;
import java.util.List;

/**
 * Class TableModel is responible for represent information about sport
 * equipment for TableView.
 *
 * @author Александра
 * @version 1.0
 */
public class TableModel {

    private List<String> headers;
    private List<List<String>> data;

    /**
     * Initialize
     */
    TableModel() {
        headers = new ArrayList<>();
        data = new ArrayList<>();
    }

    /**
     * Add text header in table.
     *
     * @param header
     */
    public void addHeader(String header) {
        headers.add(header);
    }

    /**
     * @retun headers
     */
    public List<String> getHeaders() {
        return headers;
    }

    /**
     * Add data from list of rows.
     *
     * @param goodsInfo List of rows
     */
    public void addData(List<List<String>> goodsInfo) {
        goodsInfo.forEach((info) -> {
            data.add(info);
        });
    }

    /**
     * @return data
     */
    public List<List<String>> getData() {
        return data;
    }
}
