
package testtask;

/**
 * Class Menu contains items for interface menu.
 *
 * @author Александра
 * @version 1.0
 */
enum Menu {
    SHOW_SHOP(1),
    SHOW_RENTED(2),
    RENT(3),
    EXIT(4);

    /**
     * Number of menu item.
     */
    private int itemNumber;

    /**
     * Initialize.
     *
     * @param itemNumber
     */
    private Menu(int itemNumber) {
        this.itemNumber = itemNumber;
    }

    /**
     * @return tags
     */
    public int getMenuItemNumber() {
        return itemNumber;
    }

    /**
     * Creates menu item for UI.
     *
     * @return menuItemName
     */
    @Override
    public String toString() {
        String[] words = name().split("_");
        String menuItemName = new String();
        for (String word : words) {
            menuItemName = menuItemName.concat(word + " ");
        }
        return menuItemName;
    }

}
