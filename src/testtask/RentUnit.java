
package testtask;

/**
 * Class RentUnit contains information about rent sport equipment of user.
 *
 * @author Александра
 * @version 1.0
 */
public class RentUnit {

    /**
     * All equipment rent by user.
     */
    private SportEquipment[] unit;

    /**
     * Copy of array
     *
     * @param equip
     */
    public void setUnit(SportEquipment[] equip) {
        unit = new SportEquipment[equip.length];
        System.arraycopy(equip, 0, unit, 0, equip.length);
    }

    /**
     * @return unit
     */
    public SportEquipment[] getUnit() {
        return unit;
    }

    /**
     * @param equip
     * @return 1
     */
    public int getQuantity(SportEquipment equip) {
        return 1;
    }

}
