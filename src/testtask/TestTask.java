/*
 * This application allows to keep a record of the rent of sport goods.
 *
 */
package testtask;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Main class of the application. Creates user interface and model of shop.
 *
 * @author Александра
 * @version 1.0
 */
public class TestTask {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Shop shop = new Shop();
        ShopController shopContr = new ShopController();
        shopContr.setShop(shop);

        FileParser parser = new FileParser(shop);
        parser.start();
        try {
            parser.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(TestTask.class.getName()).log(Level.SEVERE, null, ex);
        }
        MainUI ui = new MainUI(shopContr, parser.getTags());
    }
}
