
package testtask;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Class FileParser serves for parsing xml file wnich contains info about sport
 * equipment in the shop.
 *
 * @author Александра
 * @version 1.0
 */
public class FileParser extends Thread {

    /**
     * Count of tags in XML file for an equipment.
     */
    private int propertyCount = 0;

    /**
     * All tags in XML file for an equipment.
     */
    private List<String> tags;

    private Shop shop;
    private Document document;

    /**
     * Initialize.
     *
     * @param shop
     */
    FileParser(Shop shop) {
        this.shop = shop;
        tags = new ArrayList<>();
    }

    /**
     * Parse XML file with constant name.
     *
     */
    @Override
    public void run() {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            document = dBuilder.parse(XMLConstants.FILE);

            NodeList goods = document.getElementsByTagName(XMLConstants.GOOD);

            for (int i = 0; i < goods.getLength(); i++) {
                SportEquipment equip = new SportEquipment();
                equip.setTitle(getDataFromTag(i, XMLConstants.TITLE));
                equip.setPrice(Integer.parseInt(getDataFromTag(i, XMLConstants.PRICE)));
                String text = getDataFromAttribute(goods.item(i), (XMLConstants.QUANTITY));
                int quantity = Integer.parseInt(text);
                Category category = new Category();
                category.setType(getDataFromTag(i, XMLConstants.TYPE));
                category.setSport(getDataFromTag(i, XMLConstants.SPORT));
                equip.setCategory(category);

                shop.addSportEquipment(equip, quantity);
            }
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(FileParser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Search text in XML file between current open tag and current close tag.
     *
     * @param equipIndex Index of equipment in XML file
     * @param tag Name of XML tag
     * @return data Text between open tag and close tag.
     */
    private String getDataFromTag(int equipIndex, String tag) {
        NodeList nodeList = document.getElementsByTagName(tag);
        addTag(tag);
        String data = nodeList.item(equipIndex).getTextContent();
        return data;
    }

    /**
     * Add XML file tag in list of tags.
     *
     * @param tag Name of XML tag
     */
    private void addTag(String tag) {
        if (tags.size() < XMLConstants.TAGS_COUNT) {
            tags.add(tag);
        }
    }

    /**
     * Search value of attribute in XML file.
     *
     * @param node Tag that contains current attribute
     * @param attribute Attribute name of XML tag
     * @return data Attribute value
     */
    private String getDataFromAttribute(Node node, String attribute) {
        addTag(attribute);
        String data = node.getAttributes().getNamedItem(attribute).getTextContent();
        return data;
    }

    /**
     * @return tags List of differen tags in XML file
     */
    public List<String> getTags() {
        return tags;
    }
}
