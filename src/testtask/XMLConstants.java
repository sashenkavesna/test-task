
package testtask;

/**
 * Class XMLConstants contains constants for XML file.
 * 
 * @author Александра
 * @version 1.0
 */
public class XMLConstants {

    public static final String GOOD = "good";
    public static final String TITLE = "title";
    public static final String PRICE = "price";
    public static final String QUANTITY = "quantity";
    public static final String CATEGORY = "category";
    public static final String TYPE = "type";
    public static final String SPORT = "sport";

    public static final int TAGS_COUNT = 5;

    public static final String FILE = "shop.xml";
}
