
package testtask;

import java.util.List;

/**
 * Class MainUI provides main user interface.
 *
 * @author Александра
 * @version 1.0
 */
public class MainUI {

    /**
     * Initialize.
     *
     * @param shopController Controller contains shop model
     * @param tags           All different tags in XML file
     */
    MainUI(ShopController shopController, List<String> tags) {
        System.out.println(String.valueOf(Menu.SHOW_SHOP.getMenuItemNumber())
                           + ". " + Menu.SHOW_SHOP.toString());
        System.out.println(String.valueOf(Menu.SHOW_RENTED.getMenuItemNumber())
                           + ". " + Menu.SHOW_RENTED.toString());
        System.out.println(String.valueOf(Menu.RENT.getMenuItemNumber())
                           + ". " + Menu.RENT.toString());
        System.out.println(String.valueOf(Menu.EXIT.getMenuItemNumber())
                           + ". " + Menu.EXIT.toString());

        InputController inputController = new InputController(shopController.getShop(), tags);
        inputController.control();
    }
}
