
package testtask;

/**
 * Class Category contains information about kind of sport and type of sport
 * equipment.
 *
 * @author Александра
 * @version 1.0
 */
class Category {

    /**
     * Type of sport equipment.
     */
    private String type;

    /**
     * Kind of sport.
     */
    private String sport;

    /**
     * Type initialize.
     *
     * @param type Type of sport equipment
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Sport initialize.
     *
     * @param sport Kind of sport
     */
    public void setSport(String sport) {
        this.sport = sport;
    }

    /**
     * @return type Type of sport equipment
     */
    public String getType() {
        return type;
    }

    /**
     * @return sport Kind of sport
     */
    public String getSport() {
        return sport;
    }

}
