package testtask;

import java.util.List;

/**
 * Class TableView is responible for displaying information about sport
 * equipment.
 *
 * @author Александра
 * @version 1.0
 */
public class TableView {

    /**
     * Width of cell in table
     */
    private int cellWidth = 10;

    private TableModel model;

    /**
     * Set model in table.
     *
     * @param model
     */
    public void setModel(TableModel model) {
        this.model = model;
    }

    /**
     * Count space between columns.
     *
     * @param wordLength
     * @return space
     */
    private String getSpace(int wordLength) {
        String space = "";
        for (int i = 0; i < cellWidth - wordLength; i++) {
            space += " ";
        }
        return space;
    }

    /**
     * Update tabble view.
     */
    public void updateTable() {
        updateHeaders();
        updateData();
    }

    /**
     * Update All headers in table.
     */
    private void updateHeaders() {
        for (String header : model.getHeaders()) {
            System.out.print(header + getSpace(header.length()));
        }
        System.out.println("");
    }

    /**
     * Update All data in table.
     */
    private void updateData() {
        for (List<String> dat : model.getData()) {
            for (int i = 0; i < dat.size(); i++) {
                System.out.print(dat.get(i) + (getSpace(dat.get(i).length())));
            }
            System.out.println("");
        }
    }
}
