
package testtask;

/**
 * Class UIConstants contains constants for user interface.
 * 
 * @author Александра
 * @version 1.0
 */
public class UIConstants {

    public static final String INCORRECT = "Incorrect value!";
    public static final String MENU = "Use menu for continue";
    public static final String NO_RENT = "No rent";
    public static final String OK = "OK";
    public static final String MAX_RENT = "You can only rent 3 item!";
    public static final String TITLE = "Please write title of equipment or write ";
    public static final String NO_GOODS = "There is no product in the shop!";

}
