
package testtask;

/**
 * Class ShopController serves for controlling class Shop.
 *
 * @author Александра
 * @version 1.0
 */
public class ShopController {

    private Shop shop;

    /**
     * Initialize shop.
     *
     * @param shop
     */
    public void setShop(Shop shop) {
        this.shop = shop;
    }

    /**
     * @return shop
     */
    public Shop getShop() {
        return shop;
    }
}
